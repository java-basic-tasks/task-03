package com.epam.rd.java.basic.task03;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.*;
import java.util.Arrays;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class ParkingTest {

	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", delimiter = ';')
	void test(int size, String actions, String expected) throws Exception {
		Constructor<Parking> c = Parking.class.getConstructor(int.class);
		Parking p = c.newInstance(size);
		
		Arrays.stream(actions.split("\\s+"))
			.forEach(action -> {
				int place = Integer.parseInt(action.substring(action.indexOf("-") + 1));
				String actionName = nameResolver(action.charAt(0));
				
				try {
					Method m = Parking.class.getMethod(actionName, int.class);
					m.invoke(p, new Object[] { place });
				} catch (ReflectiveOperationException e) {
					e.printStackTrace();
				}
			});
		assertEquals(expected, p.toString());
	}
	
	private String nameResolver(char ch) {
		if (ch == 'a') {
			return "arrive";
		}

		if (ch == 'd') {
			return "depart";
		}
		
		return null;
	}

}

